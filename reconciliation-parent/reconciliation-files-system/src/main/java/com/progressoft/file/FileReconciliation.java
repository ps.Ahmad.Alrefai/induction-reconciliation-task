package com.progressoft.file;

import com.progressoft.con.part.*;
import com.progressoft.exception.SQLHandlingException;
import org.json.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;

public class FileReconciliation implements ReconciliationSystem<Path, Path> {
    private DataProvider<Path> source;
    private DataProvider<Path> target;

    private CSVReconciliationValidator csvReconciliationValidator = new CSVReconciliationValidator("CSV");
    private JsonReconciliationValidator jsonReconciliationValidator = new JsonReconciliationValidator("Json");
    private CSVHandler csvHandler = new CSVHandler();
    private JsonHandler jsonHandler = new JsonHandler();
    private Comparator comparator = new Comparator();
    private CompareConsumer<Path> writer;

    public FileReconciliation(DataProvider<Path> source, DataProvider<Path> target, CompareConsumer<Path> writer) throws IOException {
        this.source = source;
        this.target = target;
        this.writer = writer;

    }

    @Override
    public Path compare() throws SQLException {
        try {
            ReconciliationFactor<Path, Path> reconciliationFactor = new FileDataProviderHandler(source, target);

            // TODO those methods should be part of the ReconciliationFactor process
            reconciliationFactor.validateFactory(csvReconciliationValidator, jsonReconciliationValidator);
            reconciliationFactor.handlerFactory(csvHandler, jsonHandler);
            DataSource dataSource = reconciliationFactor.getDataSource();

            comparator.compare(source, target, dataSource, writer);

        } catch (SQLException | IOException e) {
            throw new SQLHandlingException("some thing went wrong when try to compare file", e);
        }
        return writer.getResult();
    }

    public JSONArray getMatch() {
        return comparator.getMatch();
    }

    public JSONArray getMismatch() {
        return comparator.getMismatch();
    }

    public JSONArray getMissing() {
        return comparator.getMissing();
    }


}
