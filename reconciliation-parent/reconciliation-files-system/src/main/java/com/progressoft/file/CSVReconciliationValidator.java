package com.progressoft.file;

import com.opencsv.CSVReader;
import com.progressoft.con.part.DataProvider;
import com.progressoft.con.part.ReconciliationValidator;
import com.progressoft.exception.FileHandlingException;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

public class CSVReconciliationValidator implements ReconciliationValidator<Path> {

    String dataProviderFormat;

    public CSVReconciliationValidator(String dataProviderFormat) {
        this.dataProviderFormat = dataProviderFormat;
    }

    @Override
    public String getDataProviderFormat() {
        return dataProviderFormat;
    }

    @Override
    public void validate(DataProvider<Path> dataConfiguration) {
        validateEmptyFile(dataConfiguration.getDataProviderProvenience());
    }

    private void validateEmptyFile(Path path) {
        try (Reader reader = Files.newBufferedReader(path)) {
            CSVReader csvReader = new CSVReader(reader);
            int linesNumber = 2;
            String mismatchLine = "";
            String[] line = csvReader.readNext();
            if (line == null)
                throw new IllegalArgumentException("File is Empty");
            int headCount = line.length;
            while ((line = csvReader.readNext()) != null) {
                if (line.length != headCount)
                    mismatchLine = mismatchLine + linesNumber + ",";
                linesNumber++;
            }
            if (!mismatchLine.equals(""))
                throw new IllegalArgumentException("Records " + mismatchLine + " Mismatch Headers");
        } catch (IOException e) {
            throw new FileHandlingException("incompatible file", e);
        }
    }


}
