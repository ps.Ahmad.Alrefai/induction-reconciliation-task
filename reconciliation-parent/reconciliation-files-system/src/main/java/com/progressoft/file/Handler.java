package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.progressoft.con.part.DataProviderHandler;
import com.progressoft.exception.SQLHandlingException;

import javax.sql.DataSource;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public abstract class Handler implements DataProviderHandler<Path> {
    @Override
    public void handling(DataSource dataSource, DataProvider<Path> dataProvider, String tableName) {
        validateHandlerInput(dataProvider,tableName);
        createTable(dataSource,dataProvider,tableName);
        readToTable(dataSource,dataProvider,tableName);
    }

    protected void createTable(DataSource dataSource,DataProvider<Path>dataProvider,String tableName) {
        try (Statement insertInTable = dataSource.getConnection().createStatement()) {
            insertInTable.executeUpdate(getSchemaFromDataProvider(dataProvider,tableName));
        } catch (SQLException e) {
            throw new SQLHandlingException("some thing went wrong when try to create table "+e.getErrorCode(),e);
        }
    }

    protected void insertInTable(String[] record, DataSource dataSource,String tableName) {
        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            StringBuilder insert = new StringBuilder();
            insert.append("insert into ").append(tableName).append(" values( '");
            for (int i = 0; i < record.length; i++) {
                if (i == record.length - 1)
                    insert.append(record[i]).append("' ); ");
                else
                    insert.append(record[i]).append("' , '");
            }
            statement.executeUpdate(insert.toString());

        } catch (SQLException e) {
            System.err.println(e.getErrorCode() + " : " + e.getMessage());
        }

    }

    protected abstract void readToTable(DataSource dataSource, DataProvider<Path> dataProvider, String tableName);

    private void validateHandlerInput(DataProvider<Path> dataProvider,  String tableName){
        if(dataProvider==null)
            throw new NullPointerException("null Data provider given to handler");
        if(tableName==null)
            throw new NullPointerException("null table name given to handler");
    }

    String getSchemaFromDataProvider(DataProvider<Path> dataProvider,String tableName){
        prepareSchema(dataProvider);
        StringBuilder schema = new StringBuilder();
        schema.append("CREATE TABLE ").append(tableName).append(" ( ");
        for (String element : dataProvider.getProviderContract().getContract()) {
            if (element.equals(dataProvider.getProviderContract().getIdentifier()))
                schema.append(element).append(" ").append(dataProvider.getProviderContract().getContractFiledDataType().get(element)).append(" PRIMARY KEY,");
            else
                schema.append(element).append(" ").append(dataProvider.getProviderContract().getContractFiledDataType().get(element)).append(",");
        }
        schema.deleteCharAt(schema.length() - 1);
        schema.append(");");
        return schema.toString();
    }

    private void prepareSchema(DataProvider<Path> dataProvider) {
        for (Map.Entry<String, String> entry : dataProvider.getProviderContract().getContractFiledDataType().entrySet()) {
            if (entry.getValue().equals("String"))
                entry.setValue("VARCHAR(100)");
        }
    }
}
