package com.progressoft.file;

import com.progressoft.con.part.CompareConsumer;
import com.progressoft.con.part.DataProvider;
import com.progressoft.con.part.ReconciliationComparator;

import com.progressoft.exception.SQLHandlingException;
import org.json.*;


import javax.sql.DataSource;
import java.nio.file.Path;
import java.sql.*;

public class Comparator implements ReconciliationComparator<Path,Path> {
    private JSONArray match;
    private JSONArray mismatch;
    private JSONArray missing;

    public JSONArray getMatch() {
        if(match==null)
            throw new IllegalStateException("compare method must call before getMatch");
        return match;
    }

    public JSONArray getMismatch() {
        if(mismatch==null)
            throw new IllegalStateException("compare method must call before getMismatch");
        return mismatch;
    }

    public JSONArray getMissing() {
        if(missing==null)
            throw new IllegalStateException("compare method must call before getMissing");
        return missing;
    }

    @Override
    // TODO you should introduce parameter object
    public void compare(DataProvider<Path> sourceDataProvider, DataProvider<Path> targetDataProvider, DataSource dataSource, CompareConsumer consumer) throws SQLException {
        if (dataSource == null)
            throw new NullPointerException("null dataSource when compare");
        try {
            match = getMatchTransaction(dataSource,sourceDataProvider,targetDataProvider);
            mismatch = getMismatchTransaction(dataSource,sourceDataProvider,targetDataProvider);
            missing = getMissingTransaction(dataSource,sourceDataProvider,targetDataProvider);
        } catch (SQLException e) {
            throw new SQLHandlingException("Some thing went wrong when try to get result in json format",e);
        }
        consumer.writeResult(match,mismatch,missing);
    }

    private JSONArray getMatchTransaction(DataSource dataSource, DataProvider<Path> sourceDataProvider, DataProvider<Path>targetDataProvider) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                String match = createMatchQuery(sourceDataProvider.getProviderContract().getCompareField(),
                        targetDataProvider.getProviderContract().getCompareField());
                return convertToJSON(statement.executeQuery(match));
            }
        }
    }

    private JSONArray getMismatchTransaction(DataSource dataSource, DataProvider<Path> sourceDataProvider, DataProvider<Path> targetDataProvider) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                String mismatch = createMismatchQuery(sourceDataProvider.getProviderContract().getCompareField(),
                        targetDataProvider.getProviderContract().getCompareField());
                return convertToJSON(statement.executeQuery(mismatch));
            }
        }
    }

    private JSONArray getMissingTransaction(DataSource dataSource, DataProvider<Path> sourceDataProvider, DataProvider<Path> targetDataProvider) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                String mismatch = createMissingQuery(sourceDataProvider.getProviderContract().getCompareField(),
                        targetDataProvider.getProviderContract().getCompareField());
                return convertToJSON(statement.executeQuery(mismatch));
            }
        }
    }

    private static String createMatchQuery(String[] sourceCompareField, String[] targetCompareField) {
        StringBuilder query = new StringBuilder();
        query.append("select ");
        for (String field : sourceCompareField) {
            query.append("T2.").append(field).append(",");
        }
        query.deleteCharAt(query.length() - 1);
        query.append(" from TARGET T1,SOURCE T2 where ");
        for (int fieldIndex = 0; fieldIndex < sourceCompareField.length; fieldIndex++) {
            query.append("T2.").append(sourceCompareField[fieldIndex]).append(" = ")
            .append("T1.").append(targetCompareField[fieldIndex]).append(" AND ");
        }
        query.delete(query.length() - 5,query.length()-1);
        query.append(";");
        return query.toString();
    }

    private static String createMismatchQuery(String[] sourceCompareField, String[] targetCompareField) {
        StringBuilder query = new StringBuilder();
        String sourceIdentifier = sourceCompareField[0];
        String targetIdentifier = targetCompareField[0];
        query.append("select 'Target' AS DataProviderName , ");
        for (String field : targetCompareField) {
            query.append("T1.").append(field).append(",");
        }
        query.deleteCharAt(query.length() - 1);
        query.append(" from TARGET T1,SOURCE T2 where ");
        for (int fieldIndex = 1; fieldIndex < sourceCompareField.length; fieldIndex++) {
            query.append("(T1.").append(targetIdentifier).append(" = T2.").append(sourceIdentifier);
            query.append(" And T1.").append(targetCompareField[fieldIndex]).append(" <> T2.").append(sourceCompareField[fieldIndex]).append(") or ");
        }
        query.delete(query.length() - 4,query.length()-1);
        query.append(" UNION select 'Source' AS DataProviderName , ");
        for (String field : sourceCompareField) {
            query.append("T2.").append(field).append(",");
        }
        query.append(" from TARGET T1,SOURCE T2 where ");
        for (int fieldIndex = 1; fieldIndex < sourceCompareField.length; fieldIndex++) {
            query.append("(T1.").append(targetIdentifier).append(" = T2.").append(sourceIdentifier);
            query.append(" And T1.").append(targetCompareField[fieldIndex]).append(" <> T2.").append(sourceCompareField[fieldIndex]).append(") or ");
        }
        query.delete(query.length() - 4,query.length()-1);
        query.append(";");
        return query.toString();
    }


    private static String createMissingQuery(String[] sourceCompareField, String[] targetCompareField) {
        StringBuilder query = new StringBuilder();
        String sourceIdentifier = sourceCompareField[0];
        String targetIdentifier = targetCompareField[0];
        query.append("select 'Target' AS DataProviderName , ");
        for (String field : targetCompareField) {
            query.append("T1.").append(field).append(",");
        }
        query.deleteCharAt(query.length() - 1);
        query.append(" from TARGET T1 where ").append(targetIdentifier).append(" not in (select ")
        .append(sourceIdentifier).append(" from SOURCE) UNION select 'Source' AS DataProviderName , ");
        for (String field : sourceCompareField) {
            query.append("T2.").append(field).append(",");
        }
        query.deleteCharAt(query.length() - 1);
        query.append(" from SOURCE T2 where ").append(sourceIdentifier).append(" not in (select ")
                .append(targetIdentifier).append(" from TARGET);");
        return query.toString();
    }

    private JSONArray convertToJSON(ResultSet resultSet) throws SQLException {
        JSONArray jsonArray = new JSONArray();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int total_rows = metaData.getColumnCount();
        while (resultSet.next()) {
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                obj.put(metaData.getColumnName(i + 1)
                        .toLowerCase(), resultSet.getString(i+1));
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }






}
