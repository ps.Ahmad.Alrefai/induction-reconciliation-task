package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.progressoft.con.part.ReconciliationValidator;
import com.progressoft.exception.FileHandlingException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonReconciliationValidator implements ReconciliationValidator<Path> {
    String dataProviderFormat;

    public JsonReconciliationValidator(String dataProviderFormat) {
        this.dataProviderFormat = dataProviderFormat;
    }

    @Override
    public String getDataProviderFormat() {
        return dataProviderFormat;
    }

    @Override
    public void validate(DataProvider<Path> dataProvider) {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(dataProvider.getDataProviderProvenience().toString()))
        {
            Object obj = jsonParser.parse(reader);
            JSONArray transactions = (JSONArray) obj;
            if(isEmpty(transactions))
                throw new IllegalArgumentException("File is Empty");
            if (!isValidParser(transactions))
                throw new IllegalArgumentException("File data is incompatible");
        } catch (IOException | ParseException e) {
            throw new FileHandlingException("some thing went wrong when try to validate data provider",e);
        }
    }

    private boolean isValidParser(JSONArray transactions) {
        for (Object transaction : transactions) {
            try {
                if(!isValidJsonContract(transaction))
                    return false;
                parseTransactionObject( (JSONObject) transaction );
            } catch (java.text.ParseException e) {
                System.err.println(transaction +" Can't be parsing due to : "+ e.getMessage());
            }
        }
        return true;
    }

    private boolean isEmpty(JSONArray employeeList) {
        return employeeList.size()==0;
    }

    private boolean isValidJsonContract(Object transaction){
        boolean validateJsonKeyCount = ((JSONObject) transaction).size() == 5;
        boolean validateJsonKeyContract = ((JSONObject) transaction).containsKey("date")
                                        &&((JSONObject) transaction).containsKey("reference")
                                        &&((JSONObject) transaction).containsKey("amount")
                                        &&((JSONObject) transaction).containsKey("currencyCode")
                                        &&((JSONObject) transaction).containsKey("purpose");
        return validateJsonKeyCount && validateJsonKeyContract;
    }

    private void parseTransactionObject(JSONObject transaction) throws java.text.ParseException {
        Date date=new SimpleDateFormat("dd/MM/yyyy").parse((String) transaction.get("date"));
        String reference = (String) transaction.get("reference");
        BigDecimal amount = new BigDecimal((String) transaction.get("amount"));
        String currencyCode = (String) transaction.get("currencyCode");
        String purpose = (String) transaction.get("purpose");
    }

}
