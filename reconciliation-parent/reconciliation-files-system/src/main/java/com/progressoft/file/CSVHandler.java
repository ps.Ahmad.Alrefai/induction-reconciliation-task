package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.progressoft.exception.FileHandlingException;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class CSVHandler extends Handler  {


    @Override
    protected void readToTable(DataSource dataSource, DataProvider<Path> dataProvider, String tableName){
        String line = "";
        String cvsSplitBy = ",";
        try(BufferedReader br = new BufferedReader(new FileReader(String.valueOf(dataProvider.getDataProviderProvenience())))) {
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] record = line.split(cvsSplitBy);
                insertInTable(record, dataSource,tableName);
            }
        } catch (IOException e) {
            throw new FileHandlingException("Invalid File",e);
        }
    }

    @Override
    public String getDataProviderFormat() {
        return "CSV";
    }
}
