package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.progressoft.con.part.DataProviderContract;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class FileDataProvider implements DataProvider<Path> {
    private final String format;
    private Path connectionProperty;
    private DataProviderContract contract;

    public FileDataProvider(String format, Path connectionProperty, DataProviderContract contract) throws FileNotFoundException {
        validatePath(connectionProperty);
        validateFormat(format);
        validateFileFormat(format,connectionProperty);
        validateContract(contract);
        System.out.println(connectionProperty);
        this.connectionProperty=connectionProperty;
        this.format = format;
        this.contract = contract;

    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public Path getDataProviderProvenience() {
        return connectionProperty;
    }

    @Override
    public DataProviderContract getProviderContract() {
        return contract;
    }

    private void validatePath(Path path) throws FileNotFoundException {
        Objects.requireNonNull(path, "null path");
        if (Files.notExists(path))
            throw new FileNotFoundException();
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("Not file");
    }

    private void validateContract(DataProviderContract contract){
        if(contract == null)
            throw new NullPointerException("null contract");
    }

    private void validateFormat(String format){
        if(format == null)
            throw new NullPointerException("null format");
        if(!isValidFormat(format))
            throw new IllegalArgumentException(format+" not supported");
    }

    private boolean isValidFormat(String format) {
        return format.equalsIgnoreCase("CSV") || format.equalsIgnoreCase("Json");
    }

    private void validateFileFormat(String format, Path path){
        if(!isFormatMatchExtension(format, path))
            throw new IllegalArgumentException("format not match file extension");

    }

    private boolean isFormatMatchExtension(String format, Path path) {
        String strPath = path.toString();
        String substring = strPath.substring(strPath.length() - format.length());
        return substring.equalsIgnoreCase(format);

    }



}
