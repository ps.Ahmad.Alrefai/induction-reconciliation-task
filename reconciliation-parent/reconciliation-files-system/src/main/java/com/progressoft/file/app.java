package com.progressoft.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class app {
    public static void main(String[] args) throws IOException, SQLException {
        String sourceFormat;
        String sourcePath;
        String targetFormat;
        String targetPath;
        FileContract sourceContract;
        FileContract targetContract;
        FileDataProvider sourceDataProvider;
        FileDataProvider targetDataProvider;

        Scanner scanner = new Scanner(System.in);
        System.out.println(">> Enter source file location:");
        sourcePath = scanner.next();
        System.out.println(">> Enter source file format:");
        sourceFormat = scanner.next();
        System.out.println(">> Enter target file location:");
        targetPath = scanner.next();
        System.out.println(">> Enter target file format:");
        targetFormat = scanner.next();

        //sourceContract = getCSVContract();
        sourceDataProvider = new FileDataProvider(sourceFormat, Paths.get(sourcePath), contractFactor(sourceFormat));
        //targetContract = getJsonContract();
        targetDataProvider = new FileDataProvider(targetFormat, Paths.get(targetPath), contractFactor(targetFormat));
        CSVResultWriter csvResultWriter = new CSVResultWriter();
        JsonResultWriter jsonResultWriter = new JsonResultWriter();
        FileReconciliation fileReconciliation = new FileReconciliation(sourceDataProvider,targetDataProvider,jsonResultWriter);

        System.out.println("Reconciliation finished.");
        fileReconciliation.compare().toString();


    }
    private static FileContract getCSVContract(){
        String[] sourceContract = {"transUniqueId","transDescription","amount","currecny","purpose","valueDate","transType"};
        String[] compareField = {"transUniqueId","amount","currecny","valueDate"};
        Map<String,String> contractDataType = new HashMap<>();
        contractDataType.put("transUniqueId","String");
        contractDataType.put("transDescription","String");
        contractDataType.put("amount","double");
        contractDataType.put("currecny","String");
        contractDataType.put("purpose","String");
        contractDataType.put("valueDate","String");
        contractDataType.put("transType","String");
        String identifier = "transUniqueId";
        return new FileContract(identifier,sourceContract,contractDataType,compareField);
    }

    private static FileContract getJsonContract(){
        String[] targetContract = {"date","reference","amount","currencyCode","purpose"};
        String[] compareField = {"reference","amount","currencyCode","date"};
        Map<String,String> contractDataType = new HashMap<>();
        contractDataType.put("date","String");
        contractDataType.put("reference","String");
        contractDataType.put("amount","double");
        contractDataType.put("currencyCode","String");
        contractDataType.put("purpose","String");
        String identifier = "reference";
        return new FileContract(identifier,targetContract,contractDataType,compareField);
    }

    static FileContract contractFactor(String format){
        if(format.equals("CSV"))
            return getCSVContract();
        else
            return getJsonContract();
    }
}
