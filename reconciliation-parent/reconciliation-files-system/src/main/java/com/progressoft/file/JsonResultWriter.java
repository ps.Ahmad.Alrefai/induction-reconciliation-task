package com.progressoft.file;

import com.progressoft.con.part.CompareConsumer;
import com.progressoft.exception.FileHandlingException;
import org.json.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.io.FileWriter;

public class JsonResultWriter implements CompareConsumer<Path> {

    private Path path;

    @Override
    public void writeResult(JSONArray matchResult, JSONArray mismatchResult, JSONArray missingResult) {
        String resultPath = createDir();

        try {
            writeMatchResult(matchResult);
            writeMismatchResult(mismatchResult);
            writeMissingResult(missingResult);

        } catch (IOException e) {
            throw new FileHandlingException("some thing went wrong when try to write json result",e);
        }

        path = Paths.get(resultPath).toAbsolutePath();
    }

    public Path getResult() {
        if (path == null)
            throw new IllegalStateException("You must write result first to get result path");
        return path;
    }

    private void writeMatchResult(JSONArray matchResult) throws IOException {
        File jsonFile = new File(createDir(), "match.json");
        FileWriter writer = new FileWriter(jsonFile);
        writer.write(matchResult.toString());
        writer.close();
    }

    private void writeMismatchResult(JSONArray mismatchResult) throws IOException {
        File jsonFile1 = new File(createDir(), "mismatch.json");
        FileWriter writer1 = new FileWriter(jsonFile1);
        writer1.write(mismatchResult.toString());
        writer1.close();
    }

    private void writeMissingResult(JSONArray missingResult) throws IOException {
        File jsonFile = new File(createDir(), "missing.json");
        FileWriter writer = new FileWriter(jsonFile);
        writer.write(missingResult.toString());
        writer.close();
    }


    private String createDir() {
        String path = "/home/user/reconciliation-Json-results/";
        File file = new File(path);
        file.mkdir();
        return path;
    }

}