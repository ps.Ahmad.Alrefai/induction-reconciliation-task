package com.progressoft.file;

import com.opencsv.CSVWriter;
import com.progressoft.con.part.CompareConsumer;


import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.progressoft.exception.FileHandlingException;
import org.json.*;

public class CSVResultWriter implements CompareConsumer<Path> {

    private Path path;

    @Override
    public void writeResult(JSONArray matchResult, JSONArray missMatchResult, JSONArray missingResult) throws SQLException {
        String resultPath = createDir();
        String[] matchResultHead = {"transaction id", "amount", "currecny code", "value date"};
        String[] resultHead = {"found in file", "transaction id", "amount", "currecny code", "value date"};
        try {
            createFile("match.csv",matchResultHead,matchResult,resultPath);
            createFile("mismatch.csv",resultHead,missMatchResult,resultPath);
            createFile("missing.csv",resultHead,missingResult,resultPath);
        } catch (IOException e) {
            throw new FileHandlingException("some thing went wrong when try to write file",e);
        } catch(SQLException e){
            throw new SQLException("some thing went wrong when try to write file",e);
        }

        path  = Paths.get(resultPath).toAbsolutePath();
    }
    public Path getResult() {
        if(path==null)
            throw new IllegalStateException("You must write result first to get result path");
        return path;
    }
    private void createFile(String fileName, String[] head, JSONArray resultSet, String path) throws IOException, SQLException {
        final String ResultPath = path + fileName;
        try (Writer writer = Files.newBufferedWriter(Paths.get(ResultPath))) {
            CSVWriter csvWriter = new CSVWriter(writer,
                    CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            csvWriter.writeNext(head);
            if (fileName.equals("match.csv"))
                writeMatchResult(resultSet, csvWriter);
            else if (fileName.equals("mismatch.csv"))
                writeMismatchResult(resultSet, csvWriter);
            else
                writeMissingResult(resultSet, csvWriter);
        }
    }

    private void writeMissingResult(JSONArray resultSet, CSVWriter csvWriter) throws SQLException {
        for (String[] record : getRecords(resultSet)) {
            csvWriter.writeNext(record);
        }
    }

    private void writeMismatchResult(JSONArray resultSet, CSVWriter csvWriter) throws SQLException {
        for (String[] record : getRecords(resultSet)) {
            csvWriter.writeNext(record);
        }
    }

    private void writeMatchResult(JSONArray resultSet, CSVWriter csvWriter) throws SQLException {
        for (String[] record : getRecords(resultSet)) {
            csvWriter.writeNext(record);
        }
    }


    private String createDir() {
        String path = "/home/user/reconciliation-CSV-results/";
        File file = new File(path);
        file.mkdir();
        return path;
    }



    private List<String[]> getRecords(JSONArray records){
        JSONObject jsonObject = records.getJSONObject(1);
        Iterator<String> keys;
        List<String[]> result = new ArrayList<>();
        int keyIndex ;
        for (int i = 0; i < records.length(); i++) {
            String[] csvRecord = new String[jsonObject.length()];
            jsonObject = records.getJSONObject(i);
            keys = jsonObject.keys();
            keyIndex = 0;
            String keyValue;
            while (keys.hasNext())
            {
                keyValue = keys.next();
                csvRecord[keyIndex++] = jsonObject.getString(keyValue);
            }
            result.add(csvRecord);
        }
        return result;
    }


}
