package com.progressoft.file;

import com.progressoft.con.part.*;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileDataProviderHandler implements ReconciliationFactor<Path, Path> {
    private DataProvider<Path> sourceDataProvider;
    private DataProvider<Path> targetDataProvider;
    private DataSource dataSource;

    public FileDataProviderHandler(DataProvider<Path> sourceDataProvider, DataProvider<Path> targetDataProvider) throws IOException {
        this.sourceDataProvider = sourceDataProvider;
        this.targetDataProvider = targetDataProvider;
        this.dataSource = setupDatabase();
    }

    @Override
    public void validateFactory(ReconciliationValidator<Path> sourceDataProviderValidator, ReconciliationValidator<Path> targetDataProviderValidator) {
        if(sourceDataProvider.getFormat().equals(sourceDataProviderValidator.getDataProviderFormat()))
            sourceDataProviderValidator.validate(sourceDataProvider);
        else
            targetDataProviderValidator.validate(sourceDataProvider);

        if(targetDataProvider.getFormat().equals(sourceDataProviderValidator.getDataProviderFormat()))
            sourceDataProviderValidator.validate(targetDataProvider);
        else
            targetDataProviderValidator.validate(targetDataProvider);
    }

    @Override
    public void handlerFactory(DataProviderHandler<Path> sourceDataProvideHandler, DataProviderHandler<Path> targetDataProviderHandler) {
        if(sourceDataProvider.getFormat().equals(sourceDataProvideHandler.getDataProviderFormat()))
            sourceDataProvideHandler.handling(dataSource,sourceDataProvider,"SOURCE");
        else
            targetDataProviderHandler.handling(dataSource,sourceDataProvider,"SOURCE");

        if(targetDataProvider.getFormat().equals(sourceDataProvideHandler.getDataProviderFormat()))
            sourceDataProvideHandler.handling(dataSource,targetDataProvider,"TARGET");
        else
            targetDataProviderHandler.handling(dataSource,targetDataProvider,"TARGET");
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }

    private static DataSource setupDatabase() throws IOException {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
        config.setConnectionTestQuery("VALUES 1");
        Path h2 = Files.createTempDirectory("h2");
        config.addDataSourceProperty("URL", "jdbc:h2:file:" + h2);
        config.addDataSourceProperty("user", "sa");
        config.addDataSourceProperty("password", "sa");
        return new HikariDataSource(config);
    }
}
