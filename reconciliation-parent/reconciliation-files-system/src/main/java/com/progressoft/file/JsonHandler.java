package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.progressoft.exception.FileHandlingException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class JsonHandler extends Handler {



    @Override
    public String getDataProviderFormat() {
        // TODO this is not a json
        return "CSV";
    }

    @Override
    protected void readToTable(DataSource dataSource,DataProvider<Path> dataProvider, String tableName) {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(dataProvider.getDataProviderProvenience().toString())) {
            Object obj = jsonParser.parse(reader);
            JSONArray transactions = (JSONArray) obj;
            for (Object transaction : transactions) {
                insertInTable(parseTransactionObject((JSONObject) transaction,dataProvider),dataSource,tableName);
            }
        } catch (java.text.ParseException | IOException | ParseException e) {
            throw new FileHandlingException("some thing went wrong when try to read to table",e);
        }
    }



    private String[] parseTransactionObject(JSONObject transaction,DataProvider<Path> dataProvider) throws java.text.ParseException {
        String[] record = new String[dataProvider.getProviderContract().getContract().length];
        for (int i = 0; i < record.length; i++) {
            if (i == 0)
                record[i] = reformatDate((String) transaction.get(dataProvider.getProviderContract().getContract()[i]));
            else
                record[i] = (String) transaction.get(dataProvider.getProviderContract().getContract()[i]);
        }
        return record;
    }

    private String reformatDate(String date) {
        String[] dateParts = date.split("/");
        return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }


}
