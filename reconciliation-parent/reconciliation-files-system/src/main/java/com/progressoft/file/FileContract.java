package com.progressoft.file;

import com.progressoft.con.part.DataProviderContract;

import java.util.Map;

public class FileContract implements DataProviderContract {
    String identifier;
    String[] contract;
    Map<String, String> dataContractType;
    String[] compareField;

    public FileContract(String identifier, String[] contract, Map<String, String> dataContractType, String[] compareField) {
        validateFileContractInput(identifier,contract,dataContractType);
        this.identifier = identifier;
        this.contract = contract;
        this.dataContractType = dataContractType;
        this.compareField = compareField;
    }

    private void validateFileContractInput(String identifier, String[] contract, Map<String, String> dataContractType){
        if(identifier == null)
            throw new IllegalArgumentException("null identifier");
        if(contract == null)
            throw new IllegalArgumentException("null contract");
        if(dataContractType == null)
            throw new IllegalArgumentException("null contract data type");
    }

    private void validateTableName(String tableName){
        String invalidChar = "0123456789/*-+?><\"';:!@#$%^&()_=.";
        if(tableName == null)
            throw new IllegalArgumentException("null table name");
        else for (int i = 0; i <invalidChar.toCharArray().length ; i++) {
            if(tableName.startsWith(String.valueOf(invalidChar.toCharArray()[i])))
                throw new IllegalArgumentException("Invalid table name, table can't star with "+ tableName.toCharArray()[0]);
            else if (tableName.contains(" "))
                throw new IllegalArgumentException("Invalid table name, table can't contain space");
        }
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String[] getContract() {
        return contract;
    }

    @Override
    public Map<String, String> getContractFiledDataType() {
        return dataContractType;
    }

    @Override
    public String[] getCompareField() {
        return compareField;
    }
}
