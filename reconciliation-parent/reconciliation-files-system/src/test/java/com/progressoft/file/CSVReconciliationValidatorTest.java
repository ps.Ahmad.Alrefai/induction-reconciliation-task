package com.progressoft.file;

import com.progressoft.con.part.DataProviderContract;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class CSVReconciliationValidatorTest {
    Path path;
    FileDataProvider csvfile;
    CSVReconciliationValidator csvValidator;
    String[] testSchema = new String[1];
    Map<String, String> testSchemaType = new HashMap<String, String>();
    String[] compareFiled = new String[1];
    DataProviderContract contract = new FileContract("test", testSchema, testSchemaType,compareFiled);

    @Test
    public void givenEmptyFile_whenValidate_thenThrowIllegalArgumentException() {

        try {
            Path empty = Paths.get(getClass().getClassLoader().getResource("emptyTest.csv").toURI());
            csvfile = new FileDataProvider("CSV", empty, contract);
            csvValidator = new CSVReconciliationValidator("CSV");
            IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () ->
                    csvValidator.validate(csvfile));
            Assertions.assertEquals("File is Empty", exception.getMessage());
        } catch (FileNotFoundException | URISyntaxException e) {
            Assertions.fail(e);
        }
    }

    @Test
    public void givenMismatchHeaderCSVFile_whenValidate_thenThrowIllegalArgumentException() throws URISyntaxException {
        Path mismatch = Paths.get(getClass().getClassLoader().getResource("mismatch.csv").toURI());
        try {
            csvfile = new FileDataProvider("CSV", mismatch, contract);
            csvValidator = new CSVReconciliationValidator("CSV");
            IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () ->
                    csvValidator.validate(csvfile));
            Assertions.assertEquals("Records 2,4,7, Mismatch Headers", exception.getMessage());
        } catch (FileNotFoundException e) {
            Assertions.fail(e);
            System.err.println(e.getMessage());
        }
    }

}
