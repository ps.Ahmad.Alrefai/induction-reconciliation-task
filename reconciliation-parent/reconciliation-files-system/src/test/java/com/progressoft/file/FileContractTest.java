package com.progressoft.file;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class FileContractTest {

    String identifier = "test";
    String[] contract = new String[]{"test", "test", "test"};
    Map<String, String> dataType = new HashMap<>();

    @Test
    public void givenNullIdentifier_whenConstruct_thenThrowIllegalArgument() {
        String identifier = null;
        String[] contract = new String[]{"test", "test", "test"};
        String[] compareField = new String[]{"test", "test", "test"};
        Map<String, String> dataType = new HashMap<>();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileContract fileContract = new FileContract(identifier, contract, dataType,compareField);
        });
        Assertions.assertEquals("null identifier", exception.getMessage());
    }

    @Test
    public void givenNullContract_whenConstruct_thenThrowIllegalArgument() {
        String identifier = "test";
        String[] contract = null;
        String[] compareField = new String[]{"test", "test", "test"};
        Map<String, String> dataType = new HashMap<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileContract fileContract = new FileContract(identifier, contract, dataType,compareField);
        });
        Assertions.assertEquals("null contract", exception.getMessage());
    }

    @Test
    public void givenNullContractDataType_whenConstruct_thenThrowIllegalArgument() {
        String identifier = "test";
        String[] contract = new String[]{"test", "test", "test"};
        String[] compareField = new String[]{"test", "test", "test"};
        Map<String, String> dataType = null;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileContract fileContract = new FileContract(identifier, contract, dataType,compareField);
        });
        Assertions.assertEquals("null contract data type", exception.getMessage());
    }

    @Test
    public void givenNullCompareField_whenConstruct_thenThrowIllegalArgument() {
        String identifier = "test";
        String[] contract = new String[]{"test", "test", "test"};
        String[] compareField = null;
        Map<String, String> dataType =  new HashMap<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            FileContract fileContract = new FileContract(identifier, contract, dataType,compareField);
        });
        Assertions.assertEquals("null compare data type", exception.getMessage());
    }


//    @Test
//    public void givenValidTableName_whenGetSchemaFromDataProvider_thenReturnSchema() {
//        String schema = "CREATE TABLE test ( " +
//                "ID VARCHAR(100) PRIMARY KEY," +
//                "Name VARCHAR(100));";
//        String identifier = "ID";
//        String[] contract = new String[]{"ID", "Name"};
//        Map<String, String> dataType = new HashMap<>();
//        dataType.put("ID", "String");
//        dataType.put("Name", "String");
//
//        FileContract fileContract = new FileContract(identifier, contract, dataType);
//        Assertions.assertEquals(schema, fileContract.getSchemaFromDataProvider("test"));
//    }


}
