package com.progressoft.file;


import com.progressoft.con.part.DataProviderContract;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class JsonReconciliationValidatorTest {
    String path;
    FileDataProvider jsonFile;
    JsonReconciliationValidator JsonValidator;
    String[] testSchema = new String[1];
    String[] compareField = new String[1];
    Map<String,String> testSchemaType= new HashMap<String,String>();
    DataProviderContract contract = new FileContract("test",testSchema,testSchemaType,compareField );

    @Test
    public void givenEmptyFile_whenValidate_thenThrowIllegalArgumentException() throws URISyntaxException {
        Path empty = Paths.get(getClass().getClassLoader().getResource("empty.json").toURI());
        try {
            jsonFile = new FileDataProvider("JSON",empty,contract);
            JsonValidator = new JsonReconciliationValidator("Json");
            IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,()->
                    JsonValidator.validate(jsonFile));
            Assertions.assertEquals("File is Empty",exception.getMessage());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    @Test
    public void givenIncompatibleJsonFile_whenValidate_thenThrowIllegalArgumentException() throws FileNotFoundException, URISyntaxException {
        Path mismatch = Paths.get(getClass().getClassLoader().getResource("incompatible.json").toURI());

            jsonFile = new FileDataProvider("Json",mismatch,contract);
            JsonValidator = new JsonReconciliationValidator("Json");
            IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,()->
                    JsonValidator.validate(jsonFile));
            Assertions.assertEquals("File data is incompatible",exception.getMessage());

    }
}
