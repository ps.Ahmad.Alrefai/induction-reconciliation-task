package com.progressoft.file;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ComparatorTest {



    @Test
    public void giveNullDataSource_whenCompare_thenThrowNullPointerException() throws FileNotFoundException, URISyntaxException {
        Comparator comparator = new Comparator();
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
        FileDataProvider source = new FileDataProvider("CSV",sourcePath,getSourceContract());
        FileDataProvider target = new FileDataProvider("Json",targetPath,getTargetContract());
        CSVResultWriter csvResultWriter = new CSVResultWriter();

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,()->
                comparator.compare(source,target,null,csvResultWriter));
        Assertions.assertEquals("null dataSource when compare",exception.getMessage());
    }

    @Test
    public void callGetMatch_beforeCompare_thenThrowNullPointerException() throws FileNotFoundException, URISyntaxException {
        Comparator comparator = new Comparator();
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
        FileDataProvider source = new FileDataProvider("CSV",sourcePath,getSourceContract());
        FileDataProvider target = new FileDataProvider("Json",targetPath,getTargetContract());
        CSVResultWriter csvResultWriter = new CSVResultWriter();

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, comparator::getMatch);
        Assertions.assertEquals("Cant get match result before do compare",exception.getMessage());
    }

    @Test
    public void callGetMismatch_beforeCompare_thenThrowNullPointerException() throws FileNotFoundException, URISyntaxException {
        Comparator comparator = new Comparator();
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
        FileDataProvider source = new FileDataProvider("CSV",sourcePath,getSourceContract());
        FileDataProvider target = new FileDataProvider("Json",targetPath,getTargetContract());
        CSVResultWriter csvResultWriter = new CSVResultWriter();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, comparator::getMismatch);
        Assertions.assertEquals("Cant get mismatch result before do compare",exception.getMessage());
    }

    @Test
    public void callGetMissing_beforeCompare_thenThrowNullPointerException() throws FileNotFoundException, URISyntaxException {
        Comparator comparator = new Comparator();
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
        FileDataProvider source = new FileDataProvider("CSV",sourcePath,getSourceContract());
        FileDataProvider target = new FileDataProvider("Json",targetPath,getTargetContract());
        CSVResultWriter csvResultWriter = new CSVResultWriter();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, comparator::getMissing);
        Assertions.assertEquals("Cant get missing result before do compare",exception.getMessage());
    }

    @Test
    public void givenValidDataSource_whenCompare_thenReturnResultPath() throws IOException, SQLException, URISyntaxException {
        Path path = getComparatorResult();
        File file = new File(String.valueOf(path)+"/match.csv");
        Assertions.assertTrue(file.exists());

        file = new File(String.valueOf(path)+"/mismatch.csv");
        Assertions.assertTrue(file.exists());

        file = new File(String.valueOf(path)+"/missing.csv");
        Assertions.assertTrue(file.exists());
    }


    private FileContract getSourceContract(){
        String[] sourceContract = {"transUniqueId","transDescription","amount","currecny","purpose","valueDate","transType"};
        String[] compareField = {"transUniqueId","amount","currecny","valueDate"};
        Map<String,String> contractDataType = new HashMap<>();
        contractDataType.put("transUniqueId","String");
        contractDataType.put("transDescription","String");
        contractDataType.put("amount","double");
        contractDataType.put("currecny","String");
        contractDataType.put("purpose","String");
        contractDataType.put("valueDate","String");
        contractDataType.put("transType","String");
        String identifier = "transUniqueId";

        return new FileContract(identifier,sourceContract,contractDataType,compareField);
    }

    private FileContract getTargetContract(){
        String[] targetContract = {"date","reference","amount","currencyCode","purpose"};
        String[] compareField = {"reference","amount","currencyCode","date"};
        Map<String,String> contractDataType = new HashMap<>();
        contractDataType.put("date","String");
        contractDataType.put("reference","String");
        contractDataType.put("amount","double");
        contractDataType.put("currencyCode","String");
        contractDataType.put("purpose","String");
        String identifier = "reference";

        return new FileContract(identifier,targetContract,contractDataType,compareField);
    }

    private Path getComparatorResult() throws IOException, SQLException, URISyntaxException {
        String sourceFormat = "CSV";
        String sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI()).toString();
        String targetFormat = "Json";
        String targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI()).toString();

        FileDataProvider sourceDataProvider;
        FileDataProvider targetDataProvider;

        sourceDataProvider = new FileDataProvider(sourceFormat, Paths.get(sourcePath), getSourceContract());
        targetDataProvider = new FileDataProvider(targetFormat, Paths.get(targetPath), getTargetContract());
        CSVResultWriter csvResultWriter = new CSVResultWriter();
        FileReconciliation fileReconciliation = new FileReconciliation(sourceDataProvider,targetDataProvider,csvResultWriter);

        System.out.println("Reconciliation finished.");
        return fileReconciliation.compare();
    }

    private DataSource setupDatabase() throws IOException {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
        config.setConnectionTestQuery("VALUES 1");
        Path h2 = Files.createTempDirectory("h2");
        config.addDataSourceProperty("URL", "jdbc:h2:file:" + h2);
        config.addDataSourceProperty("user", "sa");
        config.addDataSourceProperty("password", "sa");
        return new HikariDataSource(config);
    }
}
