package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class CSVHandlerTest {

    Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());

    FileContract fileContract = new FileContract("test", new String[]{""}, new HashMap<>(),new String[]{""});
    DataSource dataSource = new HikariDataSource();


    @Test
    public void givenNullInput_whenConstructCSVHandler_thenThrowNullPointerException() throws FileNotFoundException {
        DataProvider<Path> dataProvider = new FileDataProvider("CSV", this.path, fileContract);
        NullPointerException exception1 = Assertions.assertThrows(NullPointerException.class, () ->
                new CSVHandler());
        Assertions.assertEquals("null Data provider format given to handler", exception1.getMessage());
    }

    @Test
    public void givenValidInput_WhenHandlingDataProvider_thenHandlingSuccess() throws IOException, SQLException, URISyntaxException {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        FileContract fileContract = getSourceContract();
        DataSource dataSource = setupDatabase();
        FileDataProvider fileDataProvider = new FileDataProvider("CSV", path, fileContract);
        CSVHandler csvHandler = new CSVHandler();
        csvHandler.handling(dataSource,fileDataProvider,"source");
    }

    private FileContract getSourceContract() {
        String[] sourceContract = {"transUniqueId", "transDescription", "amount", "currecny", "purpose", "valueDate", "transType"};
        String[] compareField = {"reference","amount","currencyCode","date"};
        Map<String, String> contractDataType = new HashMap<>();
        contractDataType.put("transUniqueId", "String");
        contractDataType.put("transDescription", "String");
        contractDataType.put("amount", "double");
        contractDataType.put("currecny", "String");
        contractDataType.put("purpose", "String");
        contractDataType.put("valueDate", "String");
        contractDataType.put("transType", "String");
        String identifier = "transUniqueId";

        return new FileContract(identifier, sourceContract, contractDataType,compareField);
    }

    private DataSource setupDatabase() throws IOException {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
        config.setConnectionTestQuery("VALUES 1");
        Path h2 = Files.createTempDirectory("h2");
        config.addDataSourceProperty("URL", "jdbc:h2:file:" + h2);
        config.addDataSourceProperty("user", "sa");
        config.addDataSourceProperty("password", "sa");
        return new HikariDataSource(config);
    }
}
