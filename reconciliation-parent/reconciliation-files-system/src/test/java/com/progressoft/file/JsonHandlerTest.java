package com.progressoft.file;

import com.progressoft.con.part.DataProvider;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class JsonHandlerTest {
    Path path = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
    FileContract fileContract = new FileContract("test",new String[]{""},new HashMap<>(),new String[]{""});
    DataSource dataSource = new HikariDataSource();

    public JsonHandlerTest() throws URISyntaxException {
    }


    @Test
    public void givenNullInput_whenConstructHandler_thenThrowNullPointerException() throws FileNotFoundException {
        DataProvider<Path> dataProvider = new FileDataProvider("Json", this.path,fileContract);
        NullPointerException exception1 = Assertions.assertThrows(NullPointerException.class, JsonHandler::new);
        Assertions.assertEquals("null Data provider format given to handler",exception1.getMessage());

    }

    @Test
    public void givenValidInput_WhenHandlingDataProvider_thenHandlingSuccess() throws IOException, SQLException {
        FileContract fileContract = getTargetContract();
        DataSource dataSource = setupDatabase();
        FileDataProvider fileDataProvider = new FileDataProvider("Json",path,fileContract);
        JsonHandler jsonHandler = new JsonHandler();
        jsonHandler.handling(dataSource,fileDataProvider,"target");
    }

    private FileContract getTargetContract(){
        String[] targetContract = {"date","reference","amount","currencyCode","purpose"};
        String[] compareField = {"reference","amount","currencyCode","date"};
        Map<String,String> contractDataType = new HashMap<>();
        contractDataType.put("date","String");
        contractDataType.put("reference","String");
        contractDataType.put("amount","String");
        contractDataType.put("currencyCode","String");
        contractDataType.put("purpose","String");
        String identifier = "reference";

        return new FileContract(identifier,targetContract,contractDataType,compareField);
    }

    private DataSource setupDatabase() throws IOException {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
        config.setConnectionTestQuery("VALUES 1");
        Path h2 = Files.createTempDirectory("h2");
        config.addDataSourceProperty("URL", "jdbc:h2:file:"+ h2);
        config.addDataSourceProperty("user", "sa");
        config.addDataSourceProperty("password", "sa");
        return new HikariDataSource(config);
    }
}
