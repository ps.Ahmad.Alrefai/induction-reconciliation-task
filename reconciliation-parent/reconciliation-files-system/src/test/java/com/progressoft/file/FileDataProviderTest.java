package com.progressoft.file;

import com.progressoft.con.part.DataProviderContract;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;



public class FileDataProviderTest {
    String[] testSchema = new String[1];
    String[] compareField = new String[1];
    Map<String,String> testSchemaType= new HashMap<String,String>();
    DataProviderContract contract = new FileContract("test",testSchema,testSchemaType,compareField );
    DataProviderContract Nullcontract = null;

    @ParameterizedTest
    @ValueSource(strings = {"CSV", "Json"})
    public void givenNullPath_whenConstructing_thenThrowNullPointerException(String format) {
        Path path = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new FileDataProvider(format, path, contract));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"CSV", "Json"})
    public void givenNonExistPath_whenConstructing_thenThrowFileNotFoundException(String format) {
        Path path = Paths.get("test wrong path");
        Assertions.assertThrows(FileNotFoundException.class, () -> new FileDataProvider(format, path, contract));
    }

    @ParameterizedTest
    @ValueSource(strings = {"CSV", "Json"})
    public void givenDirectory_whenConstructing_thenThrowIllegalArgumentException(String format) {
        Path path = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new FileDataProvider(format, path, contract));
        Assertions.assertEquals("Not file", exception.getMessage());
    }

    @Test
    public void givenNullFormat_whenConstructing_thenThrowNullPointerException() throws URISyntaxException {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        String format = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new FileDataProvider(format, path, contract));
        Assertions.assertEquals("null format", exception.getMessage());
    }

    @Test
    public void givenInvalidFormat_whenConstructing_thenThrowNullPointerException() throws URISyntaxException {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        String format = "dume";
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new FileDataProvider(format, path, contract));
        Assertions.assertEquals(format + " not supported", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"CSV", "Json"})
    public void givenMisMatchFormat_whenConstructing_thenThrowIllegalArgumentException(String format) throws URISyntaxException {
        Path path;
        if (format.equals("CSV"))
            path = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
        else
            path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new FileDataProvider(format, path, contract));
        Assertions.assertEquals("format not match file extension", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"CSV", "Json"})
    public void givenMisMatchFormat_whenConstructing_thenThrowNullPointerException(String format) throws FileNotFoundException, URISyntaxException {
        Path path;
        if (format.equals("CSV")) {
            path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").toURI());
            FileDataProvider csvDataProvider = new FileDataProvider(format, path, contract);
            Assertions.assertEquals("CSV", csvDataProvider.getFormat());
        }
        else {
            path = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").toURI());
            FileDataProvider jsonDataProvider = new FileDataProvider(format, path, contract);
            Assertions.assertEquals("Json", jsonDataProvider.getFormat());
        }
    }

}
