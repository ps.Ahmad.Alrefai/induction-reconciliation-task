package com.progressoft.con.part;

import org.json.*;

import java.sql.SQLException;


public interface CompareConsumer<RESULT> {
    void writeResult(JSONArray matchResult, JSONArray missMatchResult, JSONArray missingResult) throws SQLException;
    RESULT getResult();
}
