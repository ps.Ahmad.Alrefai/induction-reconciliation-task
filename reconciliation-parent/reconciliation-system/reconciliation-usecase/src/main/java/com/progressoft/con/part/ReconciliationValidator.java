package com.progressoft.con.part;

public interface ReconciliationValidator<CONNECTOR> {
    String getDataProviderFormat();

    void validate(DataProvider<CONNECTOR> dataProvider);
}


