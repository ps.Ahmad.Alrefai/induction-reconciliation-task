package com.progressoft.con.part;

import javax.sql.DataSource;
import java.sql.SQLException;

public interface ReconciliationComparator<CONNECTOR,RESULT> {


    void compare(DataProvider<CONNECTOR>sourceDataProvider,
                 DataProvider<CONNECTOR>targetDataProvider,
                 DataSource dataSource,
                 CompareConsumer consumer) throws Exception;
}
