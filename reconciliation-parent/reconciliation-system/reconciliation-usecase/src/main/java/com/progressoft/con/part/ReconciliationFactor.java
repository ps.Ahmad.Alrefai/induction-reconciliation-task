package com.progressoft.con.part;

import javax.sql.DataSource;

public interface ReconciliationFactor<CONNECTOR, RESULT> {

    void validateFactory(ReconciliationValidator<CONNECTOR> sourceDataProviderValidator, ReconciliationValidator<CONNECTOR> targetDataProviderValidator);

    void handlerFactory(DataProviderHandler<CONNECTOR> sourceDataProvideHandler, DataProviderHandler<CONNECTOR> targetDataProviderHandler);

    DataSource getDataSource();
}
