package com.progressoft.con.part;

public interface DataProvider<CONNECTOR> {
    String getFormat();
    CONNECTOR getDataProviderProvenience();
    DataProviderContract getProviderContract();
}
