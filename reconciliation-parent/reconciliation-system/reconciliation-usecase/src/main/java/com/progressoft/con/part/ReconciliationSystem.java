package com.progressoft.con.part;

import java.sql.SQLException;

public interface ReconciliationSystem<CONNECTOR,RESULT> {
    RESULT compare() throws SQLException;
}
