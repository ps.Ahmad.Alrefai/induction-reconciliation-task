package com.progressoft.con.part;

import java.util.Map;

public interface DataProviderContract {
    String getIdentifier();
    String[] getContract();
    Map<String,String> getContractFiledDataType();
    String[] getCompareField();
}
