package com.progressoft.con.part;

import javax.sql.DataSource;


public interface DataProviderHandler<CONNECTOR> {
    String getDataProviderFormat();

    void handling(DataSource dataSource, DataProvider<CONNECTOR> dataProvider, String tableName);
}
