package com.progressoft.reconciliation;

import com.progressoft.con.part.CompareConsumer;
import com.progressoft.con.part.DataProvider;

public interface ReconciliationRequest<CONNECTOR,RESULT> {
    DataProvider<CONNECTOR> getSourceDataProvider();
    DataProvider<CONNECTOR> getTargetDataProvider();
    CompareConsumer<RESULT> getWriter();
}
