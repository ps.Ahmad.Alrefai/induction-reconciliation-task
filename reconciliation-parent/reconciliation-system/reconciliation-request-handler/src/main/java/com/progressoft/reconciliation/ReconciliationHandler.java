package com.progressoft.reconciliation;

// TODO rename to ReconciliationSystemFactory
public interface ReconciliationHandler<REQUEST, RESPONSE> {
    RESPONSE execute(REQUEST request);
}
