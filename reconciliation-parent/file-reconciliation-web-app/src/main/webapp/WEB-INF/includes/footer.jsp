



        <script src="${pageContext.request.contextPath}/wizard/vendor/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/wizard/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="${pageContext.request.contextPath}/wizard/vendor/jquery-validation/dist/additional-methods.min.js"></script>
        <script src="${pageContext.request.contextPath}/wizard/vendor/jquery-steps/jquery.steps.min.js"></script>
        <script src="${pageContext.request.contextPath}/wizard/vendor/minimalist-picker/dobpicker.js"></script>
        <script src="${pageContext.request.contextPath}/wizard/js/main.js"></script>

</body>
</html>