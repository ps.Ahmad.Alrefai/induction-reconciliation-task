<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="content" fragment="true" %>
<html>
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="http://localhost:8080/login/images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/login/css/main.css">
</head>
<body>

    <jsp:invoke fragment="content"/>
                    <div id="body">
                      <jsp:doBody/>
                    </div>
	<script src="${pageContext.request.contextPath}/login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/animsition/js/animsition.min.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/bootstrap/js/popper.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/select2/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="${pageContext.request.contextPath}/login/vendor/countdowntime/countdowntime.js"></script>
	<script src="${pageContext.request.contextPath}/login/js/main.js"></script>

</body>
</html>