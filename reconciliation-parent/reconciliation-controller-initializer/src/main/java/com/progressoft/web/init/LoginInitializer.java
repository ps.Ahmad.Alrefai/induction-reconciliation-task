package com.progressoft.web.init;

import app.LoginServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class LoginInitializer implements ServletContainerInitializer {

    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        LoginServlet loginServlet = new LoginServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("LoginServlet", loginServlet);
        registration.addMapping("/Login");
    }
}
