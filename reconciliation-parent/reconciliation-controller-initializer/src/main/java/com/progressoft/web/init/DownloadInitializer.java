package com.progressoft.web.init;

import app.DownloadServlet;
import app.LoginServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class DownloadInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        DownloadServlet downloadServlet = new DownloadServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("DownloadServlet", downloadServlet);
        registration.addMapping("/Download");
    }

}