package com.progressoft.web.init;

import app.ReconciliationComparatorServlet;
import com.progressoft.con.part.ReconciliationSystem;
import com.progressoft.file.FileReconciliation;
import com.progressoft.models.WebRequestHandler;
import com.progressoft.reconciliation.ReconciliationHandler;
import com.progressoft.reconciliation.ReconciliationRequest;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.nio.file.Path;
import java.util.Set;

public class ReconciliationInitializer implements ServletContainerInitializer {

    public void onStartup(Set<Class<?>> set, ServletContext ctx) throws ServletException {
        ReconciliationHandler<ReconciliationRequest<Path,Path>,
                FileReconciliation> reconciliationHandler = new WebRequestHandler();
        // TODO I need to revisit
        ReconciliationComparatorServlet reconciliation = new ReconciliationComparatorServlet(reconciliationHandler);
        ServletRegistration.Dynamic registration = ctx.addServlet("ReconciliationComparatorServlet", reconciliation);
        registration.addMapping("/Reconciliation");
    }
}
