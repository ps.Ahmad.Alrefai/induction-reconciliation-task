package com.progressoft.web.init;

import app.ReconciliationComparatorServlet;
import app.ReconciliationResultWriter;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class ReconciliationResultInitializer implements ServletContainerInitializer {

    public void onStartup(Set<Class<?>> set, ServletContext ctx) throws ServletException {
        ReconciliationResultWriter reconciliationResultWriter = new ReconciliationResultWriter();
        ServletRegistration.Dynamic registration = ctx.addServlet("ReconciliationResultWriter", reconciliationResultWriter);
        registration.addMapping("/Result");
    }
}
