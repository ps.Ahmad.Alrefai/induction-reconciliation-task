package com.progressoft.models;

import com.progressoft.con.part.CompareConsumer;
import com.progressoft.con.part.DataProvider;
import com.progressoft.exception.FileHandlingException;
import com.progressoft.file.FileReconciliation;
import com.progressoft.reconciliation.ReconciliationHandler;
import com.progressoft.reconciliation.ReconciliationRequest;

import java.io.IOException;
import java.nio.file.Path;

public class WebRequestHandler implements
        ReconciliationHandler<ReconciliationRequest<Path,Path>, FileReconciliation> {


    @Override
    public FileReconciliation execute(ReconciliationRequest<Path, Path> reconciliationRequest)  {
        DataProvider<Path> source = reconciliationRequest.getSourceDataProvider();
        DataProvider<Path> target = reconciliationRequest.getTargetDataProvider();
        CompareConsumer<Path> writer = reconciliationRequest.getWriter();

        try {
            return new FileReconciliation(source,target,writer);
        } catch (IOException e) {
            throw new FileHandlingException("Invalid data provider handling when try to handle web request",e);
        }
    }
}
