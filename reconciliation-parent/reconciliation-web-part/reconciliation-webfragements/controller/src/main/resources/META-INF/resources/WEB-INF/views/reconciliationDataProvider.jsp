<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Wizard-v1</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/wizard/css/raleway-font.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/wizard/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
	<!-- Jquery -->
	<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/wizard/css/style.css"/>
    	<script src="${pageContext.request.contextPath}/wizard/js/jquery-3.3.1.min.js"></script>
    	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    	<script src="${pageContext.request.contextPath}/wizard/js/jquery.steps.js"></script>
    	<script src="${pageContext.request.contextPath}/wizard/js/main.js"></script>
    	<script>
            $(document).ready(function () {
                $("#cancel").on("click", function () {
                   window.location.href="${pageContext.request.contextPath}/Reconciliation";
                })
            });
        </script>
</head>
<body>
	<div class="page-content" style="background-image: url('${pageContext.request.contextPath}/wizard/images/wizard-v1.jpg')">
		<div class="wizard-v1-content">
			<div class="wizard-form">
		        <form class="form-register" id="form-register" action="${pageContext.request.contextPath}/Reconciliation" method="POST" enctype="multipart/form-data" >
		        	<div id="form-total">
		        		<!-- SECTION 1 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-account"></i></span>
			            	<span class="step-number">Step 1</span>
			            	<span class="step-text">Source Infomation</span>
			            </h2>
			            <section>
			                <div class="inner">
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="SourceName">Source File Name*</label>
										<input type="text" placeholder="Source File Name" class="form-control" id="SourceName" name="sourceFileName" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="sourceFormat">Source File Format</label>
										<select name="sourceFileFormat" id="sourceFormat" class="form-control" required>
											<option value="" disabled selected>Select File Format</option>
											<option value="CSV">CSV</option>
											<option value="JSON">JSON</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group">
										<label for="sourceFile" class="form-label">File:</label>
										<div class="form-file">
											<input type="file" name="sourceFile" id="sourceFile" class="custom-file-input" required/>
										</div>
									</div>
								</div>
							</div>
			            </section>
						<!-- SECTION 2 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-card"></i></span>
			            	<span class="step-number">Step 2</span>
			            	<span class="step-text">target Infomation</span>
			            </h2>
			            <section>
							<div class="inner">
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="TargetName">Target File Name*</label>
										<input type="text" placeholder="Target File Name" class="form-control" id="TargetName" name="targetFileName" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-holder form-holder-2">
										<label for="targetFormat">Target File Format</label>
										<select name="targetFileFormat" id="targetFormat" class="form-control" required>
											<option value="" disabled selected>Select File Format</option>
											<option value="CSV">CSV</option>
											<option value="JSON">JSON</option>
										</select>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group">
										<label for="targetFile" class="form-label">File:</label>
										<div class="form-file">
											<input type="file" name="targetFile" id="targetFile" class="custom-file-input" required/>
										</div>
									</div>
								</div>
							</div>
			            </section>
			            <!-- SECTION 3 -->
			            <h2>
			            	<span class="step-icon"><i class="zmdi zmdi-receipt"></i></span>
			            	<span class="step-number">Step 3</span>
			            	<span class="step-text">Confirm Your Details</span>
			            </h2>
			            <section>
			                <div class="inner">
			                	<h3>Comfirm Details</h3>
								<div class="form-row table-responsive">
									<table class="table">
										<tbody>
											<tr class="space-row">
                                        		<th><B>Source : </B></th>
                                        	</tr>
											<tr class="space-row">
												<th>Source File Name :</th>
												<td id="sourceName"></td>
											</tr>
											<tr class="space-row">
												<th>Source File Format :</th>
												<td id="sourceFormat1"></td>
											</tr>
                                            <br>
											<tr class="space-row">
												<th><B>Target : </B></th>
											</tr>
											<tr class="space-row">
												<th>Target File Name :</th>
												<td id="targetName"></td>
											</tr>
											<tr class="space-row">
												<th>Target File Format:</th>
												<td id="targetFormat1"></td>
											</tr>
                                            <br>
										</tbody>
									</table>

								</div>
							</div>
			                                                <div class="form-row">
                                                                <div class="form-holder form-holder-2">
                                                                            <label for="resultFormat">Result Files Format</label>
                                                                            <select name="resultFormat" id="resultFormat" class="form-control" required>
                                                                                <option value="" disabled selected>Select File Format</option>
                                                                                <option value="CSV">CSV</option>
                                                                                <option value="JSON">JSON</option>
                                                                            </select>
                                                                </div>
                        									</div>
                        									<div>
                        									    <input type="submit" value = "Compare" >
                        									    <input type="button" id="cancel"  value = "Cancel">
                        									</div>
			            </section>

		        	</div>
		        </form>
			</div>
		</div>
	</div>

</body>
</html>