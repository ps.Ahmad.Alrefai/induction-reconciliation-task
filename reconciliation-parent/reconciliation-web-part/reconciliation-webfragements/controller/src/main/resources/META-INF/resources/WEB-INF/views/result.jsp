<%@page import="java.util.*" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/jquery/jquery-3.4.1.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <script src="${pageContext.request.contextPath}/popper/popper.min.js"></script>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#fileType").val("match");

            $("#Download").on("click", function () {
               $("#frmDownload").submit();
            })

            $("#otherFile").on("click", function () {
               window.location.href="${pageContext.request.contextPath}/Reconciliation";
            })
        });

        $(".nav-item").on("click",function(){
            $("#fileType").val($(this).data("value"));
        });
    </script>
</head>
<body>

<section id="tabs" class="project-tab">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" data-value="match" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                           role="tab" aria-controls="nav-home" aria-selected="true">Match</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-value="mismatch" data-toggle="tab" href="#nav-profile"
                           role="tab" aria-controls="nav-profile" aria-selected="false">Mismatch</a>
                        <a class="nav-item nav-link" id="nav-contact-tab"  data-value="missing" data-toggle="tab" href="#nav-contact"
                           role="tab" aria-controls="nav-contact" aria-selected="false">Missing</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Currency</th>
                                    <th>Amount</th>
                                    <th>Transaction ID</th>
                                    <th>Value date</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <%
                                      int recordNumber = 0;
                                      ArrayList<String[]> match=(ArrayList<String[]>) request.getAttribute("matchResult");
                                      for (String[] record : match) {
                                        recordNumber++;
                                    %>
                                      <tr>
                                      <td><%=recordNumber%></td>
                                      <%
                                          for (String field : record) {
                                      %>
                                           <td><%=field%></td>
                                      <%}%>
                                    <%}%>
                            </tbody>
                        </table>

                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Transaction ID</th>
                                    <th>Value date</th>
                                    <th>Amount</th>
                                    <th>Provider Type</th>
                                    <th>Currency</th>
                                 </tr>
                            </thead>
                            <tbody>

                                    <%
                                      int recordNumber1 = 0;
                                      ArrayList<String[]> mismatch=(ArrayList<String[]>) request.getAttribute("mismatchResult");
                                      for (String[] record1 : mismatch) {
                                        recordNumber1++;
                                    %>
                                      <tr>
                                      <td><%=recordNumber1%></td>
                                      <%
                                          for (String field1 : record1) {
                                      %>
                                           <td><%=field1%></td>
                                      <%}%>
                                    <%}%>

                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <table class="table" cellspacing="0">
                            <thead>
                                    <th>#</th>
                                    <th>Transaction ID</th>
                                    <th>Value date</th>
                                    <th>Amount</th>
                                    <th>Provider Type</th>
                                    <th>Currency</th>
                            </thead>
                            <tbody>
                                    <%
                                      int recordNumber2 = 0;
                                      ArrayList<String[]> missing = (ArrayList<String[]>) request.getAttribute("missingResult");
                                      for (String[] record2 : missing) {
                                        recordNumber2++;
                                    %>
                                      <tr>
                                      <td><%=recordNumber2%></td>
                                      <%
                                          for (String field2 : record2) {
                                      %>
                                           <td><%=field2%></td>
                                      <%}%>
                                    <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="POST" action="${pageContext.request.contextPath}/Download" id="frmDownload">
    <input type="hidden" id="fileType" name="fileType"/>
    <input type="button" id="Download" value = "Download">
    </form>
    <input type="button" id="otherFile" value = "Compare Other File">
</section>



</body>
</html>