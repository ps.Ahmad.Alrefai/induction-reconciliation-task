package app;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Paths;
import java.util.Objects;

public class DownloadServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/result.jsp");
        requestDispatcher.forward(req, response);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        String filePath = (String) req.getSession(false).getAttribute("resultFormat");
        String fileType = req.getParameter("fileType");
        String path = filePathFactory(filePath,fileType);
        File downloadFile = new File(path);
        // TODO close them correctly
        FileInputStream inStream = new FileInputStream(downloadFile);

        String relativePath = getServletContext().getRealPath("");
        System.out.println("relativePath = " + relativePath);

        downloadZip(response, downloadFile, inStream);
    }

    private void downloadZip(HttpServletResponse response, File downloadFile, FileInputStream inStream) throws IOException {
        ServletContext context = getServletContext();

        // TODO check please
        String mimeType = context.getMimeType(downloadFile.getName());
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());

        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);

        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        outStream.flush();
        inStream.close();
        outStream.close();
    }


    String filePathFactory(String fileType, String fileName){
        // TODO what absolute paths
        if(fileType.equals("JSON"))
            return "/home/user/reconciliation-Json-results/"+fileName+".json";
        else
            return "/home/user/reconciliation-CSV-results/"+fileName+".csv";
    }
}