package app;

import com.progressoft.file.FileReconciliation;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReconciliationResultWriter extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/result.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FileReconciliation reconciliation = (FileReconciliation) req.getAttribute("reconciliation");
        try {
            reconciliation.compare();
        } catch (SQLException e) {
            throw new ServletException(e);
        }

        setResultInRequest(req, reconciliation);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/result.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void setResultInRequest(HttpServletRequest req, FileReconciliation reconciliation) {
        ArrayList<String[]> match = getRecords(reconciliation.getMatch());
        ArrayList<String[]> mismatch = getRecords(reconciliation.getMismatch());
        ArrayList<String[]> missing = getRecords(reconciliation.getMissing());
        req.setAttribute("matchResult",match);
        req.setAttribute("mismatchResult",mismatch);
        req.setAttribute("missingResult",missing);
    }

    private ArrayList<String[]> getRecords(JSONArray records){
        JSONObject jsonObject;
        Iterator<String> keys;
        ArrayList<String[]> result = new ArrayList<>();
        Integer keyIndex ;
        for (int i = 0; i < records.length(); i++) {
            jsonObject = records.getJSONObject(i);
            String[] csvRecord = new String[jsonObject.length()];
            keys = jsonObject.keys();
            keyIndex = 0;
            String keyValue;
            while (keys.hasNext())
            {
                keyValue = (String)keys.next();
                csvRecord[keyIndex++] = jsonObject.getString(keyValue);
            }
            result.add(csvRecord);
        }
        return result;
    }

}
