package app;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {


    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        requestDispatcher.forward(req, resp);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
// TODO what this long method
        final String userID = "admin";
        final String password = "password";
        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");
        if (userID.equals(user) && password.equals(pwd)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", "Admin");
            session.setMaxInactiveInterval(30 * 60);
            Cookie userName = new Cookie("user", user);
            userName.setMaxAge(30 * 60);
            response.addCookie(userName);
            response.sendRedirect("/Reconciliation");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/views/login.jsp");
            PrintWriter out = response.getWriter();
            // TODO why styling here
            out.println("<font color=red>Either user name or password is wrong.</font>");
            rd.include(request, response);
        }


    }


}
