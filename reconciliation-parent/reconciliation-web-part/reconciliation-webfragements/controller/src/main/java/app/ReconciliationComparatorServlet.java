package app;

import com.progressoft.con.part.CompareConsumer;
import com.progressoft.con.part.DataProvider;
import com.progressoft.file.*;
import com.progressoft.reconciliation.ReconciliationHandler;
import com.progressoft.reconciliation.ReconciliationRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class ReconciliationComparatorServlet extends HttpServlet {

    private ReconciliationHandler<ReconciliationRequest<Path, Path>, FileReconciliation> reconciliationHandler;

    public ReconciliationComparatorServlet(ReconciliationHandler<ReconciliationRequest<Path, Path>, FileReconciliation> reconciliationHandler) {
        this.reconciliationHandler = reconciliationHandler;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/reconciliationDataProvider.jsp");
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!ServletFileUpload.isMultipartContent(request)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "This page handle multi parts forms");
            return;
        }
        try {
            List<FileItem> multiparts = new ServletFileUpload(
                    new DiskFileItemFactory()).parseRequest(request);

            FileDataProvider sourceDataProvider = getSourceDataProvider(multiparts);
            FileDataProvider targetDataProvider = getTargetDataProvider(multiparts);
            String resultFormat = multiparts.get(6).getString();
            CompareConsumer<Path> writer = writerFactory(resultFormat);
            WebRequest webRequest = new WebRequest(sourceDataProvider, targetDataProvider, writer);
            FileReconciliation reconciliation = reconciliationHandler.execute(webRequest);
            setResultFormatOnSession(request, resultFormat);

            request.setAttribute("reconciliation", reconciliation);
            request.getRequestDispatcher("/Result").forward(request, response);

        } catch (Exception ex) {
            // TODO you might have an internal error, catch specific exceptions
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        }
    }

    private void setResultFormatOnSession(HttpServletRequest request, String resultFormat) {
        HttpSession session = request.getSession();
        session.setAttribute("resultFormat", resultFormat);
        // TODO this should be part of the login
        session.setMaxInactiveInterval(30 * 60);
    }

    private FileDataProvider getTargetDataProvider(List<FileItem> multiparts) throws Exception {
        String targetFileName = multiparts.get(3).getString();
        String targetFileFormat = multiparts.get(4).getString();
        FileItem targetFileItem = multiparts.get(5);
        File targetTempFile = getFile(targetFileName, targetFileFormat, targetFileItem);
        return new FileDataProvider(targetFileFormat,
                Paths.get(targetTempFile.getPath()),
                contractFactor(targetFileFormat));
    }

    private FileDataProvider getSourceDataProvider(List<FileItem> multiparts) throws Exception {
        String sourceFileName = multiparts.get(0).getString();
        String sourceFileFormat = multiparts.get(1).getString();
        FileItem sourceFileItem = multiparts.get(2);
        File sourceTempFile = getFile(sourceFileName, sourceFileFormat, sourceFileItem);


        return new FileDataProvider(sourceFileFormat,
                Paths.get(sourceTempFile.getPath()),
                contractFactor(sourceFileFormat));
    }

    private File getFile(String name, String FileFormat, FileItem file) throws Exception {
        ;
        File tempFile = null;
        if (!file.isFormField()) {
            tempFile = File.createTempFile(name, "." + file.getName().split("\\.")[1]);
            file.write(tempFile);
        }
        return tempFile;
    }

    private static FileContract getCSVContract() {
        String[] sourceContract = {"transUniqueId", "transDescription", "amount", "currecny", "purpose", "valueDate", "transType"};
        String[] compareField = {"transUniqueId", "amount", "currecny", "valueDate"};
        Map<String, String> contractDataType = new HashMap<>();
        contractDataType.put("transUniqueId", "String");
        contractDataType.put("transDescription", "String");
        contractDataType.put("amount", "double");
        contractDataType.put("currecny", "String");
        contractDataType.put("purpose", "String");
        contractDataType.put("valueDate", "String");
        contractDataType.put("transType", "String");
        String identifier = "transUniqueId";
        return new FileContract(identifier, sourceContract, contractDataType, compareField);
    }

    private static FileContract getJsonContract() {
        String[] targetContract = {"date", "reference", "amount", "currencyCode", "purpose"};
        String[] compareField = {"reference", "amount", "currencyCode", "date"};
        Map<String, String> contractDataType = new HashMap<>();
        contractDataType.put("date", "String");
        contractDataType.put("reference", "String");
        contractDataType.put("amount", "double");
        contractDataType.put("currencyCode", "String");
        contractDataType.put("purpose", "String");
        String identifier = "reference";
        return new FileContract(identifier, targetContract, contractDataType, compareField);
    }

    static FileContract contractFactor(String format) {
        if (format.equals("CSV"))
            return getCSVContract();
        else
            return getJsonContract();
    }

    CompareConsumer<Path> writerFactory(String resultFileFormat) {
        if (resultFileFormat.equalsIgnoreCase("CSV"))
            return new CSVResultWriter();
        return new JsonResultWriter();
    }

    private class WebRequest implements ReconciliationRequest<Path, Path> {

        DataProvider<Path> source;
        DataProvider<Path> target;
        CompareConsumer<Path> writer;

        public WebRequest(DataProvider<Path> source, DataProvider<Path> target, CompareConsumer<Path> writer) {
            this.source = source;
            this.target = target;
            this.writer = writer;
        }

        @Override
        public DataProvider<Path> getSourceDataProvider() {
            return source;
        }

        @Override
        public DataProvider<Path> getTargetDataProvider() {
            return target;
        }

        @Override
        public CompareConsumer<Path> getWriter() {
            return writer;
        }
    }


}
