package com.progressoft.exception;

public class SQLHandlingException extends RuntimeException {
    public SQLHandlingException(String message, Exception e) {
        super(message,e);
    }

    public SQLHandlingException(String message) {
        super(message);
    }
}
