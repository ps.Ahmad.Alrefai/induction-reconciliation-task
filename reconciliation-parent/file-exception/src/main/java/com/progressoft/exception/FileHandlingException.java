package com.progressoft.exception;

public class FileHandlingException extends RuntimeException {
    public FileHandlingException(String message, Exception e) {
        super(message,e);
    }

    public FileHandlingException(String message) {
        super(message);
    }

}
